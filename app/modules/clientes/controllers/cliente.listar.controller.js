﻿/// <reference path="../../../lib/angular.js" />
/// <reference path="../../../lib/angular-ui-router.js" />
(function () {

	'use strict';

	//pegando o modulo app para configurar
	angular.module('app').controller('clienteListarController', clienteController);

	clienteController.$inject = ['$scope', 'clienteService'];

	function clienteController($scope, clienteService) {

		var vm = this;

		vm.ola = "Hello World!";

		//Opcao 2 : Lista Dinâmica via AJAX
		clienteService.listar().then(sucesso, erro);

		function sucesso(retorno) {
			vm.listaClientes = retorno.data;
		}

		function erro(response) {
		    alert(response.data);

		}
	}

})();
