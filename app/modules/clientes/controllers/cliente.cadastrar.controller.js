﻿/// <reference path="../../../lib/angular.js" />
/// <reference path="../../../lib/angular-ui-router.js" />
(function () {

    'use strict';

    //pegando o modulo app para configurar
    angular.module('app').controller('clienteCadastrarController', clienteController);

    clienteController.$inject = ['$scope', 'clienteService', '$state'];

    function clienteController($scope, clienteService, $state) {

        var vm = this;

        vm.cliente = {};

        this.salvar = function () {
            clienteService.cadastrar(vm.cliente).then(sucesso, erro);
        }

        function sucesso(retorno) {
            vm.cliente = {};
            $state.go('main.listar');
        }

        function erro(response) {
            alert(response.data);
        }
    }

})();
