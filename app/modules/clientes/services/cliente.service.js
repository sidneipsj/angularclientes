﻿/// <reference path="../../../lib/angular.js" />
/// <reference path="../../../lib/angular-ui-router.js" />
(function () {

    'use strict';

    //pegando o modulo app para configurar
    angular.module('app').service('clienteService', clienteService);

    clienteService.$inject = ['$http'];

    function clienteService($http) {

        this.listar = function () {
            return $http(
                {
                    url: "../api/cliente",
                    method: "GET"
                });
        }

        this.cadastrar = function (cliente) {
            return $http(
                {
                    url: "../api/cliente",
                    method: "POST",
                    data: cliente
                });
        }
    }

})();
