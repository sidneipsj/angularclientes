﻿/// <reference path="../../lib/angular.js" />
/// <reference path="../../lib/angular-ui-router.js" />
(function () {

    'use strict';

    //pegando o modulo app para configurar
    angular.module('app').config(minhaConfig);

    minhaConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    //$ palavra reservada que não obfusca
    function minhaConfig($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/listar');

        //Estado compartilhado
        $stateProvider
            .state('main',
                {
                    abstract: true,
                    templateUrl: 'modules/clientes/views/layout.html'
                })
            .state('main.listar',
                {
                    templateUrl: 'modules/clientes/views/listar.html',
                    url: '/listar'
                })
            .state('main.cadastrar',
                {
                    templateUrl: 'modules/clientes/views/cadastrar.html',
                    url: '/cadastrar'
                });
    };

})();