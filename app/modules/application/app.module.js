﻿/// <reference path="../../lib/angular.js" />
/// <reference path="../../lib/angular-ui-router.js" />

(function () {
    //padrão IIFE
    'use strict';

    //angular bootstraping (app nome comum nao obrigatorio)
    angular.module('app',
        [
            'ui.router'
        ]);

})();