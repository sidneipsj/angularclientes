﻿using AngularClientes.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace AngularClientes
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public static List<Cliente> Clientelista = new List<Cliente>();

        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
