﻿using AngularClientes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AngularClientes.Controllers
{
    public class ClienteController : ApiController
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<Cliente> Cliente()
        {
            return WebApiApplication.Clientelista;
        }

        // POST api/values
        [HttpPost]
        public void Cliente([FromBody]Cliente value)
        {
            if (value != null && !string.IsNullOrEmpty(value.Nome))
            {
                WebApiApplication.Clientelista.Add(value);
            }
        }
    }
}
