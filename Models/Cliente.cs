﻿namespace AngularClientes.Models
{
    public class Cliente
    {
        public string Nome { get; set; }
        public string Email { get; set; }
    }
}